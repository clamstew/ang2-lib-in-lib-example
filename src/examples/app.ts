import {bootstrap} from 'angular2/platform/browser';
import {Component, OnInit} from 'angular2/core';
import {Ng2Notify, Ng2NotifyService} from 'ng2-notify/notify';
import {Dragula} from 'ng2-dragula/ng2-dragula';

@Component({
	selector: 'app, div[app]',
  directives: [Ng2Notify, Dragula],
	providers: [Ng2NotifyService],
	template: `
		my app
		<ng2-notify></ng2-notify>
		<button (click)="myClickExample()">tester</button>
	`
})
class App {
	constructor(private notifyService: Ng2NotifyService) {
    // optional
    this.notifyService.config({
        corner: 'right-bottom',
        delay: 5000 // milisecons
    });
  }

  public myClickExample(message:string) {
    this.notifyService.show('default', {
      message: 'Type your message here!'
    });
  }
}

bootstrap(App, []).catch(console.error);