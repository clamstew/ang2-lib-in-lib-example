/// <reference path="../typings/main.d.ts"/>
var core_1 = require("angular2/core");
var browser_1 = require('angular2/platform/browser');
var ENV_PROVIDERS = [];
// depending on the env mode, enable prod mode or add debugging modules
if (process.env.ENV === 'prod') {
    core_1.enableProdMode();
}
else {
    ENV_PROVIDERS.push(browser_1.ELEMENT_PROBE_PROVIDERS);
}
/*
 * App Component
 * our top level component that holds all of our components
 */
var example_app_1 = require('./app/example-app');
/*
 * Bootstrap our Angular app with a top level component `App` and inject
 * our Services and Providers into Angular's dependency injection
 */
document.addEventListener('DOMContentLoaded', function main() {
    return browser_1.bootstrap(example_app_1.ExampleApp, ENV_PROVIDERS.slice())
        .catch(function (err) { return console.error(err); });
});
//# sourceMappingURL=bootstrap.js.map