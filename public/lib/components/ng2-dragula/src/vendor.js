// Polyfills
require('angular2/bundles/angular2-polyfills.js');
// Angular 2
require('angular2/platform/browser');
require('angular2/platform/common_dom');
require('angular2/core');
// Other vendors for example jQuery, Lodash or Bootstrap
// You can import js, ts, css, sass, ...
require('dragula/dragula');
//# sourceMappingURL=vendor.js.map