function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
var dragula_directive_1 = require('./src/app/directives/dragula.directive');
var dragula_provider_1 = require('./src/app/providers/dragula.provider');
__export(require('./src/app/directives/dragula.directive'));
__export(require('./src/app/providers/dragula.provider'));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    directives: [dragula_directive_1.Dragula],
    providers: [dragula_provider_1.DragulaService]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmcyLWRyYWd1bGEuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJuZzItZHJhZ3VsYS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSxrQ0FBc0Isd0NBQXdDLENBQUMsQ0FBQTtBQUMvRCxpQ0FBNkIsc0NBQXNDLENBQUMsQ0FBQTtBQUVwRSxpQkFBYyx3Q0FBd0MsQ0FBQyxFQUFBO0FBQ3ZELGlCQUFjLHNDQUFzQyxDQUFDLEVBQUE7QUFFckQ7a0JBQWU7SUFDYixVQUFVLEVBQUUsQ0FBQywyQkFBTyxDQUFDO0lBQ3JCLFNBQVMsRUFBRSxDQUFDLGlDQUFjLENBQUM7Q0FDNUIsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7RHJhZ3VsYX0gZnJvbSAnLi9zcmMvYXBwL2RpcmVjdGl2ZXMvZHJhZ3VsYS5kaXJlY3RpdmUnO1xuaW1wb3J0IHtEcmFndWxhU2VydmljZX0gZnJvbSAnLi9zcmMvYXBwL3Byb3ZpZGVycy9kcmFndWxhLnByb3ZpZGVyJztcblxuZXhwb3J0ICogZnJvbSAnLi9zcmMvYXBwL2RpcmVjdGl2ZXMvZHJhZ3VsYS5kaXJlY3RpdmUnO1xuZXhwb3J0ICogZnJvbSAnLi9zcmMvYXBwL3Byb3ZpZGVycy9kcmFndWxhLnByb3ZpZGVyJztcblxuZXhwb3J0IGRlZmF1bHQge1xuICBkaXJlY3RpdmVzOiBbRHJhZ3VsYV0sXG4gIHByb3ZpZGVyczogW0RyYWd1bGFTZXJ2aWNlXVxufVxuXG4iXX0=